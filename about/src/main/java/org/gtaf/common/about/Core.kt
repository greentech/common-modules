package org.gtaf.common.about

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.preference.PreferenceManager


const val NOT_RATED = 0
const val RATED = 1
const val WILL_NOT_RATE = 2
const val WILL_RATE_LATER = 3


fun showShareLinkDialog(context: Context, appName: String, appDetails: String, shareText: String) {
    val intent = Intent(Intent.ACTION_SEND)
    intent.type = "text/plain"
    intent.putExtra(Intent.EXTRA_SUBJECT, appName)
    intent.putExtra(Intent.EXTRA_TEXT, appDetails)
    context.startActivity(Intent.createChooser(intent, shareText))
}


fun showAboutApp(
    context: Context,
    appName: String,
    appVersion: String,
    appDetails: String,
    appIcon: Int,
    appPlayStoreLink: String
) {
    val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_about_app, null)

    val tvTitle: TextView = view.findViewById(R.id.tv_title)
    val tvDescription: TextView = view.findViewById(R.id.tv_description)
    val tvVersion: TextView = view.findViewById(R.id.tv_version)
    val icon: ImageView = view.findViewById(R.id.iv_icon)
    val devIcon: ImageView = view.findViewById(R.id.iv_dev_icon)

    tvTitle.text = appName
    tvDescription.text = appDetails
    tvVersion.text = appVersion
    icon.setImageResource(appIcon)
    icon.setOnClickListener {
        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(appPlayStoreLink)))
    }

    devIcon.setOnClickListener {
        context.startActivity(
            Intent(
                Intent.ACTION_VIEW, Uri.parse(
                    context.getString(R.string.website_link)
                )
            )
        )
    }

    val dialog = AlertDialog.Builder(context)
        .setView(view)
        .setCancelable(true)
        .create()

    val close: ImageView = view.findViewById(R.id.iv_close)
    close.setOnClickListener { dialog.dismiss() }

    dialog.window?.attributes?.gravity = Gravity.BOTTOM
    dialog.window?.setWindowAnimations(R.style.BottomSheetDialogAnimation)
    dialog.show()
    dialog.window?.setLayout(
        ViewGroup.LayoutParams.MATCH_PARENT,
        ViewGroup.LayoutParams.MATCH_PARENT
    )

}

fun showWebsite(context: Context) {
    context.startActivity(
        Intent(
            Intent.ACTION_VIEW, Uri.parse(
                context.getString(
                    R.string.contribute_link
                ).format(
                    "about_dev",
                    context.packageName
                )
            )
        )
    )
}


fun showContributionDialog(context: Context) {
    val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_contribute, null)

    val btnContribute = view.findViewById<Button>(R.id.btn_contribute)
    btnContribute.setOnClickListener {
        context.startActivity(
            Intent(
                Intent.ACTION_VIEW, Uri.parse(
                    context.getString(
                        R.string.contribute_link
                    ).format(
                        "contribution_dialog",
                        context.packageName
                    )
                )
            )
        )
    }

    val dialog = AlertDialog.Builder(context)
        .setView(view)
        .setCancelable(true)
        .create()

    val close = view.findViewById<View>(R.id.iv_close)
    close.setOnClickListener { dialog.dismiss() }

    dialog.window?.attributes?.gravity = Gravity.BOTTOM
    dialog.window?.setWindowAnimations(R.style.BottomSheetDialogAnimation)
    dialog.show()
    dialog.window?.setLayout(
        ViewGroup.LayoutParams.MATCH_PARENT,
        ViewGroup.LayoutParams.MATCH_PARENT
    )
}


fun showRatingDialog(context: Context, ratePageLink: String, forceShow: Boolean = false) {
    val ratingPreference =
        getRatingPreference(context)
    if (forceShow || ratingPreference != RATED && ratingPreference != WILL_NOT_RATE) {
        AlertDialog.Builder(context)
            .setTitle(context.getString(R.string.rating_title))
            .setMessage(context.getString(R.string.rating_message))
            .setCancelable(false)
            .setPositiveButton(context.getString(R.string.rate)) { dialogInterface, _ ->
                setRatingPreference(
                    RATED,
                    context
                )
                val intent = Intent("android.intent.action.VIEW")
                intent.data = Uri.parse(ratePageLink)
                dialogInterface.dismiss()
                context.startActivity(intent)
            }
            .setNeutralButton(context.getString(R.string.rate_later)) { dialogInterface, _ ->
                setRatingPreference(
                    WILL_RATE_LATER,
                    context
                )
                dialogInterface.dismiss()
            }
            .setNegativeButton(context.getString(R.string.rate_nothanks)) { dialogInterface, _ ->
                setRatingPreference(
                    WILL_NOT_RATE,
                    context
                )
                dialogInterface.dismiss()
            }
            .show()
    }
}

fun showUpdateDialog(context: Context, marketLink: String, onUpdateClick: () -> Unit) {
    AlertDialog.Builder(context)
        .setTitle(context.getString(R.string.new_version_available))
        .setMessage(context.getString(R.string.please_update_to_new_version))
        .setCancelable(false)
        .setPositiveButton(
            context.getString(R.string.update)
        ) { dialog, _ ->
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(marketLink))
            context.startActivity(intent)
            onUpdateClick()
        }
        .show()
}


private fun setRatingPreference(i: Int, context: Context) {
    PreferenceManager.getDefaultSharedPreferences(context).edit()
        .putInt("show_rating_dialog", i).apply()
}


private fun getRatingPreference(context: Context): Int {
    return PreferenceManager.getDefaultSharedPreferences(context)
        .getInt("show_rating_dialog", NOT_RATED)
}
package org.gtaf.about

import android.app.Activity
import android.app.Instrumentation
import android.content.Context
import android.content.Intent
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.isInternal
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import org.hamcrest.Matchers.not
import org.hamcrest.core.AllOf.allOf
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.LooperMode
import org.robolectric.annotation.TextLayoutMode

@RunWith(AndroidJUnit4::class)
@MediumTest
@LooperMode(LooperMode.Mode.PAUSED)
@TextLayoutMode(TextLayoutMode.Mode.REALISTIC)
class UtilityFunctionTest {

    val context = ApplicationProvider.getApplicationContext<Context>()

    @Before
    fun stubAllExternalIntents() {

//        intending(not(isInternal())).respondWith(Instrumentation.ActivityResult(RESULT_OK, null))
        intending(not<Intent>(isInternal())).respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, null))

    }

    @Test
    fun ensureLinkSharingWorks() {
        shareLink(context)

        intended(allOf(hasAction(Intent.ACTION_SEND)))
    }

    @Test
    fun ensureAppSharingWorks() {
        shareApk(context)

        intended(allOf(hasAction(Intent.ACTION_SEND)))
    }

    @Ignore
    @Test
    fun textCopyFunctionWorks() {


    }
}
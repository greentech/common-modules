package org.gtaf.common

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import org.gtaf.common.about.showContributionDialog
import kotlinx.android.synthetic.main.activity_main.*
import org.gtaf.common.about.BuildConfig
import org.gtaf.common.about.showShareLinkDialog
import org.gtaf.common.about.showWebsite

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    fun showContribute(view: View) {
        showContributionDialog(this)
    }

    fun shareApp(view: View) {
        showShareLinkDialog(
            this, "Dummy App", "Lorem ipsum dot siment. " +
                    "Lorem ipsum dot siment. Lorem ipsum dot siment. " +
                    "Lorem ipsum dot siment. Lorem ipsum dot siment.", "Lorem ipsum"
        )
    }

    fun showAboutDev(view: View) {
        showWebsite(this)
    }

    fun showAboutApp(view: View) {
        org.gtaf.common.about.showAboutApp(
            this,
            "App Title",
            BuildConfig.VERSION_NAME,
            "Lorem ipsum dot set. Lorem ipsum dot set. Lorem ipsum dot set. Lorem ipsum dot set. Lorem ipsum dot set. ",
            R.mipmap.ic_launcher,
            "https://play.google.com"
        )
    }

    fun showRatingDialog(view: View) {
        org.gtaf.common.about.showRatingDialog(
            this,
            "https://example.com",
            true
        )
    }

    fun showUpdateDialog(view: View) {
        org.gtaf.common.about.showUpdateDialog(
            this,
            "market://details?id=com.greentech.quran"
        ) {
            finish()
        }
    }

}

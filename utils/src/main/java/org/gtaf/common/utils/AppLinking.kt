package org.gtaf.common.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AlertDialog

class AppLinking(private val context: Context, private val links: List<String> ) {

    constructor(context: Context, string: String): this(
        context, string.split(",")
    )

    private val appNames = context.resources.getStringArray(R.array.app_names)
    private val marketLinks = context.resources.getStringArray(R.array.urls)

    fun startIntent(i: Int) {
        val bundle = Bundle()
        val app = links[i].split(":")
        when (app[0].toInt()) {
            HADITH, HADITH_C, HADITH_S -> {
                bundle.putInt(COLLECTION_ID, app[1].toInt())
                bundle.putString(BOOK_ID, "" + app[2])
                bundle.putInt(HADITH_SELECTED, app[3].toInt() - 1)

                val intent = Intent(HADITH_INTENT)
                intent.putExtras(bundle)
                startApp(intent, i)
            }
            QURAN -> {
                bundle.putInt(SURA, app[1].toInt())
                bundle.putInt(AYA, app[2].toInt())

                val intent = Intent(QURAN_INTENT)
                intent.putExtras(bundle)
                startApp(intent, i)
            }
            HISNUL -> {
                bundle.putInt(GROUP, app[1].toInt())
                bundle.putInt(CHILD, app[2].toInt())

                val intent = Intent(HISNUL_INTENT)
                intent.putExtras(bundle)
                startApp(intent, i)
            }
            NARRATOR -> {
                bundle.putInt(GROUP, app[1].toInt())
                bundle.putInt(CHILD, app[2].toInt())

                val intent = Intent(SCHOLARS_INTENT)
                intent.putExtras(bundle)
                startApp(intent, i)
            }
        }
    }

    private fun startApp(intent: Intent, i: Int) {

        try {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            AlertDialog.Builder(context)
                    .setTitle("Need to download app " + appNames[i] + " for this")
                    .setMessage("Want to download?")
                    .setPositiveButton(
                        android.R.string.ok
                    ) { dialogInterface, _ ->
                        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(marketLinks[i])))
                    }
                    .setNegativeButton(android.R.string.cancel
                    ) { dialog, _ -> dialog.dismiss() }
                    .show()

        }

    }

    companion object {

        const val PAGING = "PAGING"
        const val IDNUM = "idnum"
        const val CURSOR_VIEWTYPE = "cursortype"
        const val QUERY = "query"

        const val QURAN = 1
        const val HADITH = 2
        const val HISNUL = 3

        const val HADITH_C = 9
        const val HADITH_S = 8

        const val GROUP = "group"
        const val CHILD = "child"
        const val NARRATOR = 4
        const val SURA = "SURA"
        const val AYA = "AYA"

        const val QURAN_INTENT = "quran.intent.action.QURAN"
        const val HISNUL_INTENT = "hisnulmuslim.intent.action.Dua"
        const val HADITH_INTENT = "hadith.intent.action.HADITH"
        const val SCHOLARS_INTENT = "scholars.intent.action.SCHOLARS"

        const val COLLECTION_ID = "collection_id"
        const val HADITH_SELECTED = "hadithselected"
        const val BOOK_ID = "book_id"
        const val NARRATOR_ID = "rowid"
        const val VIEW_INTENT = "android.intent.action.VIEW"
    }

}
